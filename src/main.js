// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill';
import Vue from 'vue';
import App from './App';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import router from './router/router';
import store from './vuex';
import './router/guard';
import './mixins';
import Cropper from 'cropperjs';
import CropperJsVue from 'cropperjs-vue';
import { morphUpperCase } from 'vue-morphling';
import VImgFallback from 'v-img-fallback';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'vue-awesome/icons/comment';
import 'vue-awesome/icons/brands/android';
import 'vue-awesome/icons/brands/facebook-f';
import Icon from 'vue-awesome/components/Icon';

Vue.component('fa-icon', Icon);

Vue.use(VImgFallback, {
  loading: '',
  error: ''
});

Vue.use(CropperJsVue, Cropper);

Vue.use(morphUpperCase);

Vue.use(Vuetify, {
  theme: {
    primary: '#29B6F6',
    secondary: '#039BE5',
    accent: '#FF5722',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#4caf50'
  },
  iconfont: 'fa'
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
