export const base64toBlob = (base64) => {
  var imageBase64 = base64.split(',')[1];
  var binaryImg = window.atob(imageBase64);
  var length = binaryImg.length;
  var ab = new ArrayBuffer(length);
  var ua = new Uint8Array(ab);
  for (var i = 0; i < length; i++) {
    ua[i] = binaryImg.charCodeAt(i);
  }
  return new window.Blob([ab], {
    type: 'image/jpeg'
  });
};

export const fileToBase64 = (file) => {
  var reader = new window.FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    console.log(reader.result);
  };
  reader.onerror = function (error) {
    console.log('Error: ', error);
  };
};
