import Vue from 'vue';
import settings from '../../settings';

Vue.mixin({
  created () {
    this.$settings = settings;
  },
  data () {
    return {
      $settings: null
    };
  },
  methods: {
    $mockRequest: (success = true, timeout) => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (success) resolve();
          reject(new Error({message: 'There was an error!'}));
        }, timeout);
      });
    }
  },
  computed: {
    $binding () {
      const binding = {};
      const name = this.$vuetify.breakpoint.name;
      if (name === 'xs' || name === 'sm') binding.column = true;
      if (this.$vuetify.breakpoint.mdAndUp) binding.row = true;
      return binding;
    },
    $isMobile () {
      return this.$vuetify.breakpoint.mdAndDown;
    }
  }
});
