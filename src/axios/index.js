import axios from 'axios';

export const signup = async (payload) => {
  const request = axios.create({
    baseURL: process.env.BASE_URL
  });
  console.log(payload);
  const { data } = await request.post(`/user/signup`, payload);
  return data;
};

export const create = async (payload) => {
  const request = axios.create({
    baseURL: process.env.BASE_URL
  });
  const { data } = await request.post(`/user/create`, payload);
  return data;
};
