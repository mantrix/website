import Vue from 'vue';
import Router from 'vue-router';
import home from '@/components/home';
import terms from '@/components/terms';
import privacyPolicy from '@/components/privacy-policy';
import signup from '@/components/signup';
import pricing from '@/components/pricing';
import main from '@/components/dashboard/main';
import dashboard from '@/components/dashboard/dashboard';
import adminMain from '@/components/admin/main';
import adminDashboard from '@/components/admin/dashboard';
import profile from '@/components/dashboard/profile';
import payment from '@/components/dashboard/payment';
import website from '@/components/dashboard/website';
import store from '../vuex';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/main',
      name: 'main',
      component: main,
      redirect: 'main/dashboard',
      meta: {
        requiresAuth: true,
        admin: false
      },
      children: [
        {
          path: '/main/dashboard',
          name: 'dashboard',
          component: dashboard,
          meta: {
            requiresAuth: true,
            admin: false
          }
        },
        {
          path: '/main/profile',
          name: 'profile',
          component: profile,
          meta: {
            requiresAuth: true,
            admin: false
          }
        },
        {
          path: '/main/payment',
          name: 'payment',
          component: payment,
          meta: {
            requiresAuth: true,
            admin: false
          }
        }
      ]
    },
    {
      path: '/main/website',
      name: 'website',
      component: website,
      meta: {
        requiresAuth: true,
        admin: false
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: adminMain,
      redirect: 'adminDashboard',
      meta: {
        requiresAuth: true,
        admin: true
      },
      children: [
        {
          path: '/admin/dashboard',
          name: 'adminDashboard',
          component: adminDashboard,
          meta: {
            requiresAuth: true,
            admin: true
          }
        }
      ]
    },
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/signup',
      name: 'signup',
      component: signup
    },
    {
      path: '/pricing',
      name: 'pricing',
      component: pricing
    },
    {
      path: '/terms',
      name: 'terms',
      component: terms
    },
    {
      path: '/privacy-policy',
      name: 'privacy-policy',
      component: privacyPolicy
    },
    {
      path: '/logout',
      name: 'logout',
      async beforeEnter (to, from, next) {
        await store.dispatch('auth/signout');
        store.dispatch('auth/wipe');
        store.dispatch('user/wipe');
        store.dispatch('storage/wipe');
        next({name: 'home'});
      }
    }
  ]
});
