import router from './router';
import { currentUser$ } from '../rxjs';
// import store from '../vuex';

router.beforeEach((to, from, next) => {
  if (!to.matched.some(m => m.meta.requiresAuth)) {
    next();
    return;
  }

  // const user = store.getters['user/user'];

  currentUser$().subscribe({
    next: (currentUser) => {
      if (currentUser) {
        // NOTE: For early release only
        if (to.path !== '/main/website') {
          next('/main/website');
          return;
        }
        next();

        // NOTE: Bring this code below back
        // console.log(user.isAdmin);
        // if (user.isAdmin) {
        //   next();
        // } else {
        //   if (user && user.subscription && !user.subscription.paid) {
        //     console.log(to.path);
        //     if (to.path !== '/main/payment') {
        //       next('/main/payment');
        //       return;
        //     }
        //     next();
        //   }
        //   next();
        // }
      } else {
        next('/');
      }
    },
    error: e => console.error(e)
  });
});
