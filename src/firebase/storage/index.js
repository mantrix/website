import { storage } from '../exports';
import { Observable } from 'rxjs/Observable';
import { base64toBlob } from '../../utils/blob';
import firebase from 'firebase';

export const uploadFile = (path, file) => {
  return Observable.create(obs => {
    const blob = base64toBlob(file);

    var uploadTask = storage.ref()
      .child(path)
      .child('e-signature.jpg')
      .put(blob);

    uploadTask.on('state_changed', (snapshot) => {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED:
          // console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING:
          // console.log('Upload is running');
          break;
      }
      obs.next({
        done: false,
        progress
      });
    }, (error) => {
      obs.error(error);
    }, () => {
      uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
        obs.next({
          done: true,
          url: downloadURL
        });
      });
    });
  });
};
