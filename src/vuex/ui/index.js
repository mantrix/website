import { ref } from '../../firebase';
import uid from 'uid';

const state = {
  apkLink: ''
};

const getters = {
  apkLink: (s) => s.apkLink
};

const actions = {
  getAPKLink ({ commit }) {
    ref.child('_config')
      .child('apkLink')
      .on('value', snap => {
        commit('setApkLink', snap.val());
      }, err => {
        throw err;
      });
  }
};

const mutations = {
  setApkLink (s, val) {
    s.apkLink = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
