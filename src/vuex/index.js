import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import auth from './auth';
import excel from './excel-version';
import user from './user';
import storage from './storage';
import ui from './ui';

Vue.use(Vuex);

const modules = {
  auth,
  excel,
  user,
  storage,
  ui
};

export default new Vuex.Store({
  modules,
  plugins: [createPersistedState()]
});
