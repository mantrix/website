import { storage } from '../../firebase';

function dataURItoBlob (dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = window.atob(dataURI.split(',')[1]);

  // separate out the mime component
  // var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var bb = new window.Blob([ab]);
  return bb;
}

const state = {
  progress: 0
};

const getters = {
  progress: (s) => s.progress
};

const actions = {
  upload: ({ commit }, {type, uid, data}) => {
    return new Promise((resolve, reject) => {
      const blob = dataURItoBlob(data);
      const path = `photos/${uid}/profile_pic_${uid}`;
      const task = storage.ref()
        .child(path)
        .put(blob);

      task.on('state_changed', snap => {
        let progress = (snap.bytesTransferred / snap.totalBytes) * 100;
        commit('setProgress', progress);
      }, error => {
        reject(error);
      }, () => {
        task.snapshot.ref.getDownloadURL().then(function (url) {
          resolve(url);
        });
      });
    });
  },
  wipe: ({commit}) => {
    commit('setProgress', 0);
  }
};

const mutations = {
  setProgress: (s, val) => { s.progress = val; }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
