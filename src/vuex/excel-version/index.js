import { ref } from '../../firebase';
import uid from 'uid';

const state = {};

const getters = {};

const actions = {
  createOrder: async ({commit}, payload) => {
    let id = uid(7).toUpperCase();
    // let snap = null;
    // do {
    // id = uid(7);
    // snap = await ref.child('excel-orders')
    // .child(id)
    // .once('value');
    // } while (snap.exists());
    await ref.child('excel-orders')
      .child('data')
      .child(id)
      .set({
        createdAt: new Date().getTime(),
        status: 'pending',
        amount: 1999,
        ...payload
      });
    await ref.child('excel-orders')
      .child('total')
      .transaction(total => {
        if (!total) return 1;
        return ++total;
      });
    return id;
  },
  getOrder: async ({commit}, id) => {
    const snap = await ref.child('excel-orders')
      .child('data')
      .child(id)
      .once('value');
    if (!snap.exists()) { throw new Error('invalid-order-code'); }
    console.log(snap.val());
    return snap.val();
  },
  addOrderPicUrl: async ({commit}, {id, url}) => {
    await ref.child('excel-orders')
      .child('data')
      .child(id)
      .child('url')
      .set(url);
    await ref.child('excel-orders')
      .child('data')
      .child(id)
      .child('status')
      .set('payment-sent');
  }
};

const mutations = {
  setUser: (s, val) => { s.user = val; },
  setDiscounts: (s, val) => { s.discounts = val; }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
