import { ref } from '../../firebase';

const state = {
  user: {
    isAdmin: false
  },
  discounts: null
};

const getters = {
  user: (s) => s.user,
  discounts: (s) => s.discounts
};

const actions = {
  getUser: ({commit}, uid) => {
    ref.child('users')
      .child('data')
      .child(uid)
      .on('value', snap => {
        const data = snap.val();
        if (!data.contacts) {
          data.contacts = {
            workEmail: '',
            phone: {
              work: '',
              workMobile: ''
            },
            social: {
              facebook: '',
              twitter: '',
              linkedIn: '',
              googlePlus: '',
              other: ''
            }
          };
        }
        commit('setUser', data);
        return data;
      }, error => {
        console.error(error);
      });
  },
  getDiscounts: ({commit}) => {
    ref.child('discounts')
      .on('value', snap => {
        commit('setDiscounts', snap.val());
      }, error => {
        console.error(error);
      });
  },
  updateUser: async ({commit}, {uid, data}) => {
    const updates = {};
    updates[`websiteEnabled`] = true;
    updates[`picURL`] = data.picURL || '';
    updates[`url`] = data.url;
    updates[`bio`] = data.bio || '';
    updates[`name/prefix`] = data.name.prefix;
    updates[`name/firstName`] = data.name.firstName;
    updates[`name/middleName`] = data.name.middleName || '';
    updates[`name/lastName`] = data.name.lastName;
    updates[`searchIndexFirstName`] = data.name.firstName.toLowerCase();
    updates[`searchIndexMiddleName`] = data.name.middleName.toLowerCase();
    updates[`searchIndexLastName`] = data.name.lastName.toLowerCase();
    updates[`contacts`] = data.contacts;

    const urlSnap = await ref.child('users')
      .child('data')
      .orderByChild('url')
      .equalTo(data.url)
      .once('value');

    if (urlSnap.exists()) {
      urlSnap.forEach(child => {
        if (child.key !== uid) {
          throw new Error({message: `The url "${data.url}" already exists. Please pick a unique one.`});
        }
      });
    }

    await ref.child('users')
      .child('data')
      .child(uid)
      .update(updates);
  },
  wipe: ({commit}) => {
    commit('setUser', {
      isAdmin: false
    });
    commit('setDiscounts', null);
  },
  addEducation: async ({commit}, {uid, data}) => {
    await ref.child('users')
      .child('data')
      .child(uid)
      .child('educations')
      .push(data);
  },
  removeEducation: async ({commit}, {uid, key}) => {
    await ref.child('users')
      .child('data')
      .child(uid)
      .child('educations')
      .child(key)
      .set(null);
  },
  addService: async ({commit}, {uid, data}) => {
    await ref.child('users')
      .child('data')
      .child(uid)
      .child('services')
      .push(data);
  },
  removeService: async ({commit}, {uid, key}) => {
    await ref.child('users')
      .child('data')
      .child(uid)
      .child('services')
      .child(key)
      .set(null);
  }
};

const mutations = {
  setUser: (s, val) => { s.user = val; },
  setDiscounts: (s, val) => { s.discounts = val; }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
