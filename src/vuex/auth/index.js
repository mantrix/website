import { auth } from '../../firebase';
import { signup, create } from '../../axios';
import { Observable } from 'rxjs/Observable';
import firebase from 'firebase';

const state = {
  currentUser: null
};

const getters = {
  currentUser: (s) => s.currentUser
};

const actions = {
  currentUser$: ({commit}) => {
    return Observable.create(obs => {
      auth.onAuthStateChanged(user => {
        console.warn('auth state changed', !!user);
        commit('setCurrentUser', user);
        obs.next(user);
      });
    });
  },
  signup: async ({commit, dispatch}, data) => {
    const payload = data;
    await signup(payload);
    await dispatch('login', {email: data.email, password: data.password});
  },
  login: async ({commit}, {email, password}) => {
    const { user } = await auth.signInWithEmailAndPassword(email, password);
    commit('setCurrentUser', user);
    return user;
  },
  googleSign: async ({commit}) => {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    const user = await firebase.auth().signInWithPopup(provider);
    // await auth.signOut();
    return user;
  },
  facebookSign: async ({commit}) => {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.setCustomParameters({
      'display': 'popup'
    });
    const { user } = await firebase.auth().signInWithPopup(provider);
    commit('setCurrentUser', user);
    return user;
  },
  createUser: async ({commit}, {user, data}) => {
    const payload = {
      user,
      data
    };
    await create(payload);
  },
  signout: async ({commit, dispatch}) => {
    await auth.signOut();
    dispatch('wipe');
  },
  sendEmailVerification: async ({commit}) => {
    await auth.currentUser.sendEmailVerification();
  },
  wipe: ({commit}) => {
    commit('setCurrentUser', null);
  }
};

const mutations = {
  setCurrentUser: (s, val) => { s.currentUser = val; }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
