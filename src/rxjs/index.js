import { auth } from '../firebase';
import { Observable } from 'rxjs/Observable';

export const currentUser$ = () => {
  return Observable.create(obs => {
    auth.onAuthStateChanged(user => {
      obs.next(user);
    });
  });
};
