const settings = require('../../settings');

module.exports = [
  {
    'path': '/',
    'title': `${settings.websiteName} | Seamless construction estimation app thsystem system that works offline.`,
    'description': 'Seamless construction estimation app thsystem system that works offline.',
    'image': ''
  },
  {
    'path': '/signup',
    'title': `${settings.websiteName} | Signup`,
    'description': 'Create an account.',
    'image': ''
  },
  {
    'path': '/pricing',
    'title': `${settings.websiteName} | FREE Trial`,
    'description': 'Pricing page',
    'image': ''
  },
  {
    'path': '/terms',
    'title': `${settings.websiteName} | Terms`,
    'description': 'Terms of Use',
    'image': ''
  },
  {
    'path': '/terms',
    'title': `${settings.websiteName} | Privacy Policy`,
    'description': 'Privacy Policy',
    'image': ''
  }
];
